class Article{
    constructor(id , titre, date , contenu){
        this.id = id;
        this.titre = titre;
        this.date = date;
        this.contenu = contenu;
    }
}
const texte = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquet eget sit amet tellus cras adipiscing enim eu. Diam maecenas sed enim ut sem viverra aliquet eget. Iaculis at erat pellentesque adipiscing commodo elit. Pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Ullamcorper malesuada proin libero nunc consequat. Sit amet nisl purus in mollis nunc. Blandit cursus risus at ultrices mi tempus imperdiet. Pretium fusce id velit ut tortor pretium viverra suspendisse potenti. Non arcu risus quis varius quam quisque id diam. Diam vulputate ut pharetra sit. Dui id ornare arcu odio. Cursus turpis massa tincidunt dui ut. Dictum varius duis at consectetur lorem. Commodo ullamcorper a lacus vestibulum. Consectetur lorem donec massa sapien faucibus et molestie ac. Lacinia at quis risus sed vulputate.In ante metus dictum at. Facilisis gravida neque convallis a. Vestibulum sed arcu non odio euismod lacinia at quis risus. Elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at. Volutpat sed cras ornare arcu dui vivamus arcu. Magna ac placerat vestibulum lectus mauris ultrices eros. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Aliquam id diam maecenas ultricies mi eget mauris. Id faucibus nisl tincidunt eget nullam non nisi est. In tellus integer feugiat scelerisque varius. Dolor magna eget est lorem ipsum. Risus in hendrerit gravida rutrum quisque. Maecenas ultricies mi eget mauris pharetra et ultrices neque ornare. Neque gravida in fermentum et sollicitudin. Aliquam malesuada bibendum arcu vitae. Sem integer vitae justo eget magna fermentum."

const articleList = [
    new Article(0 , "Article 1" , "02 juin 2017" , texte),
    new Article(1 , "Article 2" , "12 juin 2020" , texte),
    new Article(2 , "Article 3" , "28 avril 2015" , texte),
    new Article(3 , "Article 4" , "16 juillet 2020" , texte),
    new Article(4 , "Article 5" , "11 janvier 2017" , texte),
    new Article(5 , "Article 6" , "5 mars 2019" , texte),
    new Article(6 , "Article 7" , "30 décembre 2020" , texte),
    new Article(7 , "Article 8" , "20 octobre 2020" , texte),
    new Article(8 , "Article 1" , "6 août 2018" , texte),
    new Article(9 , "Article 10" , "18 septembre 2020" , texte)
];

exports.getArticles = () => {
    return articleList;
}

exports.getArticleById = (id) =>{
    let article;
    for(let i=0 ; i < articleList.length ; i++){
        if(articleList[i]["id"] == id){
            article = articleList[i];
        }
    }
    return article;
}