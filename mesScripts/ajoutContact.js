class Contact {
    constructor (id, nom, prenom, courriel, telephone, sujet, champ) {
      this.id = id
      this.nom = nom
      this.prenom = prenom
      this.courriel = courriel
      this.telephone = telephone
      this.sujet = sujet
      this.champ = champ
    }
  }

const users = [];
  exports.addUser = (user) => {
    users.push(new Contact(users.length , user.nom , user.prenom , user.courriel , user.telephone , user.sujet , user.champ));
    return users;
  }