
class Formations
{
    constructor(id , nom , description , prix , type){
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.type = type;
    }
}

const listeFormations = [
    new Formations("0" , "Initiation Mongo Db" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor" , "40$" , "mongo"),
    new Formations("1" , "Initiation Node Js" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit,consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor sed do eiusmod tempor" , "55$" , "node"),
    new Formations("2" , "Maîtriser Les bases de Node Js" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor" , "30$" , "node"),
    new Formations("3" , "Les bases du Mongo DB" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor" , "199$" , "mongo"),
    new Formations("4" , "Devenir expert en NoSql" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor" , "70$" , "mongo"),
    new Formations("5" , "Node Js : From Zero to Hero" , "Lorem ipsum dolor sit amet, consectetur adipisicing consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor elit, sed do eiusmod tempor" , "28$" , "node"),
    new Formations("6" , "Découvrez Express js" , " consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor" , "60$" , "node"),
    new Formations("7" , "Professional projects in Mongo Db" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor" , "49$" , "mongo"),
    new Formations("8" , "Professional projets in Node Js" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor do eiusmod tempor" , "120$" , "node"),
    new Formations("9" , "Migrez vers Mongo Db" , "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod temporconsectetur adipisicing elit, sed do eiusmod tempor" , "67$" , "mongo"),
];

exports.getFormationsByType = (type) =>{
    const titreFormations = [];
    for(let i=0 ; i < listeFormations.length ; i++){
        if(listeFormations[i]["type"] == type){
            titreFormations.push(listeFormations[i]);
        }
    }
    return titreFormations;
}

exports.getFormationsById = (id) =>{
    let formation;
    for(let i=0 ; i < listeFormations.length ; i++){
        if(listeFormations[i]["id"] == id){
            formation = listeFormations[i];
        }
    }
    return formation;
}
