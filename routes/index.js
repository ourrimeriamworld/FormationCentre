var express = require('express');
var router = express.Router();
let formationsList = require('../mesScripts/formations.js');
let blogModule = require('../mesScripts/blog.js');
let contact = require('../mesScripts/ajoutContact.js');
const { check, validationResult } = require('express-validator');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Centre de Formations' });
});

router.get('/index', function(req, res, next) {
  res.render('index', { title: 'Accueil' });
});

router.get('/formations', function(req, res, next) {
  res.render('formations', { title: 'Nos formations' });
});

router.get('/mongo', function(req, res, next) {
  res.render('mongo', { title: 'Mongo DB' , tabFormations : formationsList.getFormationsByType("mongo")});
});

router.get('/detailsFormation/:id', function(req, res, next) {
  res.render('detailsFormation', { title: 'Détails formation' , detailFormation : formationsList.getFormationsById(req.params.id)});

});

router.get('/node', function(req, res, next) {
  res.render('node', { title: 'Node Js' , tabFormations : formationsList.getFormationsByType("node")});
});

router.get('/blog' , function(req , res , next){
  res.render('blog' , {title : 'Blog' , listeArticle : blogModule.getArticles()})
});

router.get('/detailArticle/:id' , function(req , res , next){
  res.render('detailArticle' , {title : 'Détails Article' , article : blogModule.getArticleById(req.params.id)})
});

router.get('/contact', function(req, res, next) {
  res.render('contact', { title: 'Contact'});
});

router.post('/contact',[
  check('nom','Le nom doit contenir 3 lettres minimum').isLength({min: 3}),
  check('prenom','Le prénom doit contenir 3 lettres minimum').isLength({min: 3}),
  check('courriel' , 'Le courriel est invalide').isEmail()
], function(req, res, next) {
  const errors = validationResult(req);

  if(!errors.isEmpty()){
    res.render('contact', {erreursForms : errors.array(), formData: {
      nom: req.body.nom,
      prenom: req.body.prenom,
      courriel: req.body.courriel
    }});
  }
  else{
    let contactList = contact.addUser(req.body);
    res.render('contact', { title: 'Contact' , nouveauContact : req.body});
    console.log(contactList);
  }
  
});
module.exports = router;
